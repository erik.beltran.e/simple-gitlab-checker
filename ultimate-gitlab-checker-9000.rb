# yes is a funny name and 0 dependency
require "open-uri"
require 'net/http'
alert_seconds=300

while true
	uri = URI.parse("https://gitlab.com")
	http = Net::HTTP.new(uri.host, uri.port)
	http.use_ssl = true
	start_time = Time.now
	http.get(uri.request_uri)
	elapsed_time = Time.now - start_time
	if elapsed_time > alert_seconds
		puts "The response time exceeded the parameter established, Time taked: #{elapsed_time} , Max.  #{alert_seconds} "
	end
end